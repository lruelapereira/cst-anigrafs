/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste_condorcet;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lruel
 */

public class Teste_main {

    public static void main(String[] args) {
        List<Vertex> graph = new ArrayList<>();

//        graph.add(new Vertex("S", 7, 5, 10));
//        graph.add(new Vertex("Q", 0, 2, 8));
//        graph.add(new Vertex("A", 5, 0, 7));
//        graph.add(new Vertex("T", 8, 3.5f, 4));
//        graph.add(new Vertex("R", 3, 4, 0));
        graph.add(new Vertex("M", 0, 0, 42));
        graph.add(new Vertex("N", 0, 10, 26));
        graph.add(new Vertex("K", 0, 18, 17));
        graph.add(new Vertex("C", 0, 15, 15));
        // Edge[] -> Vertex, dist

        graph.get(0).Condorcet(graph);
    }
}

