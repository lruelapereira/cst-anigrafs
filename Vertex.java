package teste_condorcet;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class OrderCollections implements Comparator<Vertex> {
    @Override
    public int compare(Vertex a, Vertex b) {
        return b.eval - a.eval;
    }
}

class Vertex{

    public String name;
    // -> Double
    public float x, y;
    public int eval;
    
    public void Condorcet(List<Vertex> graph)
    {
        Collections.sort(graph, new OrderCollections());

        float m_dist[][] = new float[graph.size()][graph.size()];
        int z = 0;
      
        for (int i = 0; i < graph.size(); i++) {
            for (int j = i+1; j < graph.size(); j++) {
                m_dist[i][j] = (float) Math.pow(((float) Math.pow(graph.get(i).x - graph.get(j).x, 2) + (float) Math.pow(graph.get(i).y - graph.get(j).y, 2)), 1.0 / 2.0);
                m_dist[j][i] = m_dist[i][j];
            }
            m_dist[i][i] = 0;
        }
        // Dijkstra's algorithm
        // Atualiza a matriz m_dist
        
        int m_contest[] = new int[((int) Math.pow(graph.size(), 2) - graph.size()) / 2];
        int votes[][] = new int[graph.size()][2]; // votes[0][] -> number of wins, votes[1][] -> number of total votes

        for (int i = 0; i < graph.size(); i++) {
            for (int j = i+1; j < graph.size(); j++) {
                for (int k = 0; k < graph.size(); k++) {
                    if (m_dist[k][i] < m_dist[k][j]) {
                        m_contest[z] += graph.get(k).getEval();
                        votes[i][1] += graph.get(k).getEval();
                    }
                    if (m_dist[k][i] > m_dist[k][j]) {
                        m_contest[z] -= graph.get(k).getEval();
                        votes[j][1] += graph.get(k).getEval();
                    }
                }
                if (m_contest[z] > 0) {
                    votes[i][0] += 1;
                }
                if (m_contest[z] < 0) {
                    votes[j][0] += 1;
                }
                z += 1;
            }
        }
        z = 0;
        for (int i = 1; i < graph.size(); i++) {
            if (votes[i][0] == votes[z][0]){
                if (votes[i][1] > votes[z][1])
                    z = i;
            }   
            if (votes[i][0] > votes[z][0])
                z = i;
        }
        System.out.print("vencedor eh ");
        System.out.println(graph.get(z).name);
    }

    public Vertex(String name, float x, float y, int eval) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.eval = eval;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getEval() {
        return eval;
    }

    public void setEval(int eval) {
        this.eval = eval;
    }
}